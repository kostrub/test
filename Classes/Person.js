const Shop = require('./Shop')

class Person {
    constructor(name, age, mood, time) {
        this._name = name;
        this._age = age;
        this._mood = mood;
        this._time = time * 8;
        this._happiness = 0
        this._money = 0
    }
    

// Getters

    get name() {
        return this._name
    }
    get age() {
        return this._age
    }
    get mood() {
        return this._mood
    }
    get time() {
        return `${this._name} has ${Math.round(this._time / 8)} days and ${this._time % 8} hours or (${this._time}) working hours left.`
    }
    get happiness() {
        return this._happiness
    }
    get money() {
        return `${this._name} has £${this._money.toFixed(2)} money`
    }

// Setters
    set name(newName) {
        this._name = newName;
    }
    set age(newAge) {
        this._age = newAge
    }
    set mood(newMood) {
        this._mood = newMood
    }
    set happiness(newLevel) {
        this._happiness = newLevel
    }

    work(hours){
        this._money += (hours * 7.85)
        this._time -= hours


    }

}


module.exports = Person;

// const Nata = new Person('Kakashka', 26, 'neurtral', 7)

// console.log(Nata.age)
// console.log(Nata.age)
// console.log(Nata.mood)
// console.log(Nata.happiness)
// console.log(Nata.money)
// Nata.work(7)
// console.log(Nata.money)
// console.log(Nata.time);

// newShop = new Shop('Prada', 7)
// console.log(newShop.menu);